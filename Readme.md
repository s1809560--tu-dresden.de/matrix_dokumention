# TU Dresden Matrix Dokumentation
## contribute

### get the project and run the server
```sh
git clone --recursive git@gitlab.hrz.tu-chemnitz.de:s1809560--tu-dresden.de/matrix_dokumention.git
cd matrix_dokumention
hugo server
```

### requirements
The documentation is built with [Hugo](https://gohugo.io/).
It is tested with hugo v0.74.3. You can install hugo from your package manager.
