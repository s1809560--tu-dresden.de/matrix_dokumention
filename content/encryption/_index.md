---
title: "Verschlüsselung"
date: 2020-07-03T13:20:58+02:00
draft: false
chapter: true
weight: 60
---

# Ende-zu-Ende-Verschlüsselung nutzen

1:1 Gespräche sind seit Kurzem standardmäßig Ende-zu-Ende-verschlüsselt. Daher wird eine eingerichtete Schlüsselsicherung sowie eine Verifikation aller selbst eingesetzten Client-Geräte empfohlen ([Erste Schritte]({{< ref "first-steps" >}}))

Die Entscheidung, ob ein erzeugter Raum derartig verschlüsselt werden soll muss gut überlegt sein und kann nicht mehr rückgängig gemacht werden. Wenn es sich um größere oder öffentliche Räume handelt, könnte das Prüfen aller Schlüssel aller Gesprächspartner viel Zeit in Anspruch nehmen. Diese manuelle Prüfung kann man aber auch später bei Gelegenheit durchführen und auch direkt Ende-zu-Ende-verschlüsselte Gespräche mit vorerst blindem Vertrauen starten.

Wer in einem Raum eine Ende-zu-Ende-Verschlüsselung (E2EE) zu Gesprächspartner:innen wünscht, erreicht dies durch Klick auf die Raumeinstellungen (z.B. über das Zahnrad oben):

![neuer Raum ohne Verschlüsselung](14_Direktnachricht5.webp)

Dazu ist im Reiter Sicherheit & Datenschutz der Schieberegler Verschlüsselt zu bewegen:

![Verschlüsselung in den Raumeinstellungen aktivieren](14_Direktnachricht6.webp)

Dies kann mit OK bestätigt werden. Ab sofort können die Nachrichten nur noch von den am Gespräch Beteiligten gelesen werden. Sofern nicht schon vorab in den Einstellungen eine Schlüsselsicherung eingerichtet wurde, wird dies hier nochmals vorgeschlagen durchzuführen. So können auch bei Verlust des Gerätes, Schließen des Tabs o.a. Vorfälle die Schlüssel für frühere Gespräche wiederverwendet werden. Sie sind mit dem Master-Passwort (NICHT ZIH-Passwort!) auf dem Server der TU Dresden verschlüsselt gespeichert:

![Hinweis zum Sichern der Raumschlüssel](14_Direktnachricht7.webp)

Nun kann der verschlüsselte Austausch beginnen. Wenn man sich von der Korrektheit der Schlüssel überzeugen, und diese Vertrauenswürdigkeit digital dokumentieren möchte, ist dazu zuerst auf das Personensymbol oben rechts die Seitenleiste auszuklappen:

![öffnen der Personenliste in dem Raum](14_Direktnachricht8.webp)

In der sich öffnenden Leiste der am Gespräch Beteiligten kann nun auf die Kontaktperson geklickt werden:

![Raum mit Hinweis, dass nicht alle eilnehmenden verifiziert sind.](14_Direktnachricht9.webp)

Die Leiste zeigt jetzt die Gesprächsperson im Detail an. Hier ist eine Schlüsselverifikation durch Klick auf „Verifizieren“ einleitbar:

![Menü zu der zu verifizierenden Person mit dem Verifizieren-Knopf ausgewählt](16_E2EE1.webp)

Dies muss mit einem Klick auf „Start Verification“ bestätigt werden:

![Menü zum starten der Verifikation](16_E2EE2.webp)

Diese Verifizierung sollte mit der Kontaktperson durch Abgleich (z.B. mündlichen via Telefon, im selben Zimmer o.a. Medium) geschehen. Da dies nicht immer leicht ist, kann auch ersteinmal das Vertrauen ausgesprochen werden (sonst wird man immer wieder gefragt, die Verifizierung durchzuführen) und bei Gelegenheit (bspw. beim nächsten Meeting) durchzuführen.

Eine ausführliche Darstellung des Themas findet sich in diesem [Video von Prof. Plikat](https://invidious.ggc-project.de/VOxfa6dqXSk) sowie [im diesem Matrix-Blogartikel](https://blog.riot.im/e2e-encryption-by-default-cross-signing-is-here).

Der Abgleich selbst geschieht über Emoji-Bildchen oder QR-Codes, die je nach Gerät und Iconpack verschieden aussehen können. Auch ist die Übersetzung aller Oberflächenelemente ins Deutsche nicht 100%-ig vorhanden.

![Emojivergleich zum verifizieren des Schlüsselaustauschs](16_E2EE.webp)

Analog geschieht dies in Räumen mit mehreren Teilnehmenden, jeweils einzeln. 

![Verifikationsprozess war erfolgreich](verified.png)

In den jeweiligen Raumzeilen deuten folgende Symbole den Status der Verschlüsselung und der dazugehörigen Verifikation an:

![Symbol für mindestens eine nicht-verifizierte Person](gray.png)

Mindestens eine Person im Raum wurde noch nicht verifiziert.

![Symbol für eine verifizierte Person, die unverifizierte Sitzungen geöffnet hat](unverified.png)

Im Raum ist mindestens eine Person, die bereits verifiziert wurde, aber die ihrerseits weitere unverifizierte Sitzungen geöffnet hat. 

![Symbol für alle Personen im Raum sind verifiziertß](green.png)

Alle im Raum befindlichen Personen wurden verifiziert.



