---
title: "Erste Schritte"
date: 2020-08-02T21:26:25+02:00
chapter: true
draft: false
weight: 2
---

# Erste Schritte - Wie kann Matrix genutzt werden?

Mitgliedern und Angehörigen der TU Dresden (selbstverständlich auch Studierenden) wird unter Einhaltung der einschlägigen gesetzlichen und rechtlichen Bestimmungen zum Datenschutz und zur IT-Sicherheit ermöglicht, mittels ihres **ZIH-Logins** mit Angehörigen dieser und anderer Universitäten sowie weiteren Matrix-Nutzenden (bspw. akademischen Partner:innen) per Chat sowie Audio-/Video-Telefonie zu
kommunizieren.

Starten Sie hier: [https://matrix.tu-dresden.de](https://matrix.tu-dresden.de) 

![Startseite von Element Webclient mit Anmeldebutton](01_Welcome.webp)

Hierzu ist keine Registrierung nötig, der Dienst kann sofort durch Klick auf „Anmelden“ auf der Startseite [https://matrix.tu-dresden.de](https://matrix.tu-dresden.de) genutzt werden.

![Loginfenster mit Aufforderung ZIH Login und Passwort einzugeben](02_Login1_de.png)

Das Dropdown-Menü „Anmelden mit:“ sollte auf „Benutzername“ belassen werden. Dann sind folgende Eingaben zu tätigen:

**Benutzername: ZIH-Login**  (nur der ZIH-Login, keine E-Mail-Adresse!)

**Passwort: ZIH-Passwort**

Ein alternativer Login, bspw. über die E-Mail-Adresse ist **NICHT** beim ersten, initialen, Anmelden möglich, erst ab dem zweiten Einloggen.

Es folgt nach dem Erstlogin auch keine E-Mail / Bestätigungsmail.

Analog zu E-Mail-Adressen ergeben sich damit Matrix-Adressen folgender Struktur:

@ZIH-Login:tu-dresden.de

⚠️ Sollten Sie statt mit der oben genannten Website (Element Web-App an der TU Dresden installiert) sofort mit einem [Matrix Client](clients.md) starten wollen, ist es wichtig den Heimserver vom zumeist standardmäßig eingestellten matrix.org auf https://matrix.tu-dresden.de zu ändern (dargestellt in den folgenden drei Bildschirmfotos):

![Anmeldeseite mit Fokus auf dem Homeserver ändern Button](02_Login2_de.png)

1. Klick auf Ändern

![Eingabefeld zum Ändern des Homeserers mit der Eingabe mstrix.tu-dresden.de](02_Login3_de.png)

2. Markieren der voreingestellten Heimserver-Adresse und entfernen dieser.

![](02_Login4_de.png)

3. Eintragung der Matrix-Heimserver-Adresse der TU Dresden

Eine Registrierung von Accounts (wie vllt. von anderen Matrix-Servern bekannt) ist hier an der TU Dresden nicht möglich, da den Dienst ausschließlich Personen mit ZIH-Login nutzen können. Die TU Dresden ist kein Kommunikationsdiensteanbieter. 

Für wissenschaftliche Kooperationen mit Kolleg:innen ohne ZIH-Account besteht die Möglichkeit zur [Beantragung eines ZIH-Gast-Accounts](https://tu-dresden.de/zih/dienste/service-katalog/zugangsvoraussetzung), welcher auch zur Nutzung von Matrix berechtigt.

Die Förderation mit den Matrix-Servern anderer wissenschaftlicher oder zivilgesellschaftlicher Institutionen ist jedoch in Kürze möglich (analog zur bestehenden E-Mail-Föderation). Beispiel-Server, mit denen jetzt schon kommuniziert werden kann sind:

* [TU München](https://tum.de)

* [Uni Hamburg](http://uni-hamburg.de/)

* [Uni Heidelberg](https://uni-heidelberg.de/)

* [Uni Bochum](http://ruhr-uni-bochum.de)

* [TU Freiberg](https://tu-freiberg.de)

* [TU Chemnitz](http://tu-chemnitz.de)

* [Hochschule Weimar](http://bau-ha.us)

* [University for Business and Technology, Kosovo](https://ubt-uni.net/)

Für die zivilgesellschaftliche Nutzung des Protkolls Matrix gibt es hier eine Liste an öffentlichen Heimservern, die auch von Kolleg:innen genutzt werden können, falls ihre Institution noch keinen Matrix-Server anbietet:
[https://www.hello-matrix.net/public_servers.php](https://www.hello-matrix.net/public_servers.php)

Datenschutzerklärung: https://matrix.tu-dresden.de/datenschutz.html

Impressum: https://matrix.tu-dresden.de/impressum.html

