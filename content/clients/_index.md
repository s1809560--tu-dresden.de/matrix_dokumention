---
title: "Clients/Installation"
date: 2020-07-02T21:22:27+02:00
draft: false
chapter: true
weight: 15
---

# Installation eines Clients / Programms

Der empfohlene Client zur Nutzung von Matrix heißt Element (früher Riot) und kann auf verschiedenen Systemen und Geräten genutzt werden:
1. Element Web: Web-App, die an der TU Dresden installiert ist und direkt im Browser geöffnet wird: 
[https://matrix.tu-dresden.de](https://matrix.tu-dresden.de)
2. Element Desktop: [Downlaods für Linux/Windows/Mac](https://element.io/get-started)
3. Element Mobil: [Android/iOS](https://app.element.io/mobile_guide/)

[Download Element](https://element.io/get-started)

![Element Seite zum Download des Clients](12_Element-Download.png)


## Element Web
Der einfachste Weg ist das direkte Öffnen der Element Web Anwendung in einem modernen Browser (z.B. [Mozilla Firefox](https://www.mozilla.org/de/firefox/)) unter der Adresse: [https://matrix.tu-dresden.de](https://matrix.tu-dresden.de).


## Element Desktop

[Downlaods für Linux/Windows/Mac](https://element.io/get-started)

Empfehlenswerter als die Nutzung eines Browsertabs ist jedoch die Installation des Programms Element auf dem eigenen Rechner. Hier kann unabhängig vom Browser der Überblick behalten werden (allerdings sollte man sich auch um die Aktualisierungen des Programms kümmern). 

Nach einer Desktop-Installation ist darauf zu achten, den bestehenden Account mit dem ZIH-Login zu nutzen, und keinen neuen Account auf einem anderen Server zu erstellen. Hier am Beispiel von Element:

![makierter Anmeldebutton im Element Matrixclient](17_Anmeldung_de.png)

Dies wird durch Klick auf **Ändern** realisiert. Dann landet man nicht versehentlich auf einem falschen Server...

![Anmeldeseite mit Fokus auf dem Homeserver ändern Button](17_MatrixOrg_de.png)

Nun kann man manuell die Angabe des Heimservers durchführen: https://matrix.tu-dresden.de

![Eingabefeld zum Ändern des Homeservers mit der Eingabe mstrix.tu-dresden.de](17_Heimserver_de.png)

Anschließend ist der einmalige Login mit ZIH-Login und ZIH-Passwort durchzuführen:

![Loginfenster mit Aufforderung ZIH Login und Passwort einzugeben](17_ZIH-Login_de.png)

Mit der Aktivierung des Schiebereglers unter Einstellungen > Einstellungen > „**Nach System-Login automatisch starten**“ startet der Element (früher Riot)-Client nach jedem Neustart und man verpasst keine Benachrichtigungen mehr durch ein versehentliches Schließen des Browser-Tabs in der Nutzungsvariante mit der Web-App.

![einstellungen mit dem Punkt nach Systemstart automatisch starten makiert](13_System-Login.webp)


## Element Mobil

Download Element Mobil: [Android/iOS](https://app.element.io/mobile_guide/). 
Die folgende Bilderreihe zeigt Bildschirmfotos der Einrichtung von Android Element:

![Screenshot der Anmeldeprozedur unter Android/iOS](15_Android1.png)

![Screenshot des Element Androidclients nach erfolgreichem Einloggen](15_Android2.png)



