---
title: "Räume finden"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
---
## Räume finden

Das Raumverzeichnis, in dem die öffentlich zugänglichen Räume präsentiert werden, erreicht man über den Butten „Entdecke / Explore“ links oben unter dem eigenen Anzeigenamen. Hier kann sofort nach einem Raum auf dem TUD-Matrix gesucht werden.

Die globale Föderation befindet sich aktuell im Testbetrieb. Es kann sein, dass die Föderation zu späterem Zeitpunkt wieder eingeschränkt wird.

