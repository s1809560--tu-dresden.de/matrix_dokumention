---
title: "Räume teilen"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
---
# Räume teilen und publik machen

Jeder Raum hat eine Adresse, die man in den Raumeinstellungen unter dem Reiter Erweitert finden kann, bspw.

!aen6iekahv8Pi0zohf:tu-dresden.de

Da diese kryptische Adresse aber von Menschen nicht leicht gelesen werden kann, können Raumadressen vergeben werden, die von Menschen gelesen werden können. Dies ist nur in Räumen nötig, die man an anderen Stellen publik machen möchte.

Entweder durch global veröffentlichte Adressen (auffindbar von Benutzenden in anderen Servern - sinnvoll bei Themen, die über die TU Dresden hinausgehen) oder durch eine lokale Adresse, die nur innerhalb des Matrix-Heimatservers an der TU Dresden gilt.

Für den häufigeren Fall der gewünschten lokalen Adresse in die Raumeinstellungen unter dem Reiter Allgemein auf „Show more“ unter Local Addresses klicken:

![Raumeinstellungen mit dem mehr anzeigen ausgewählt](rooms7.webp)

Anschließend  kann in der „Room alias“ Zeile ein wiedererkennbarer Name dieser Raumverknüpfung vergeben werden (keine Leerzeichen!):

![Raumeinstellungen mit dem lokale Adressen ausgewählt](rooms8.webp)

Es können auch verschiedene Adressen vergeben werden. Sollte die Raumadresse im Raum-Verzeichnis des Matrix-Heimatservers der TU Dresden veröffentlicht werden sollen, kann dies durch den folgend gezeigten Schalter und die Auswahl der Primären Adresse im Drop-Down-Menü erfolgen:

![Raumeinstellungen mit dem öffentliche Raumadresse ausgewählt](rooms9.webp)

Die Raumadresse hat dann folgende Struktur

#Raumadressname:tu-dresden.de

In einem Chat, in dem man auf diesen Raum aufmerksam machen möchte, lässt sich auf einen Raum, der eine lokale Adresse hat, durch beginnendes Tippen von #Raumadressname...  ein Hyperlink zum Raum erzeugen, den man mit einem Mausklick bestätigen muss.

Weiterhin resultiert auf der vergebenen Raumadresse eine Internetadresse (URL)

https://matrix.tu-dresden.de/#/room/#Raumadressname:tu-dresden.de

Diese kann leicht in der Öffentlichkeit bzw. an die Zielgruppe verteilt werden.

![Teilensymbol in der Chatansicht des Raums makiert](room_sharing.webp)

{{% notice warning %}}
Das Teilen-Symbol oben rechts in jedem Raum, bietet auch einen Link an, sowie einen QR-Code und verschiedene soziale Netzwerke. Der Link führt allerdings auf https://matrix.to/ von wo aus man den Raum über eine Element Web-App von https://element.io angeboten bekommt. Dies kann man machen, aber für angesprochene Eingeladene aus der TU Dresden führt der obige Link (mit tu-dresden beinhaltend) direkt in die Element Web-App, die an der TU Dresden installiert ist. Der matrix.to-Link wäre insb. für außeruniversitäre Kooperationspartner:innen interessant, die den Raum in ihren Matrix-Clients öffnen wöllten oder eben in Element.
{{% /notice %}}


