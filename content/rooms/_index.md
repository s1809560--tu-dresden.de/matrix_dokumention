---
title: "Räume"
date: 2020-07-02T21:23:14+02:00
chapter: true
draft: false
weight: 35
---
## Räume erstellen und Verantwortung übernehmen

Neue Räume werden über das + in der linken Leiste in der Kategorie Räume erstellt.

![Makierung des Raumhinzufügenbuttons](rooms1.webp)
Anschließend ist der Raumname zu vergeben. Auch kann optional ein Thema (das später öfter angepasst werden kann) vergeben werden. Optional kann der Raum auch öffentlich zugänglich gemacht werden (dies ist nicht die Standardeinstellung). Weiterhin kann verhindert werden, dass Matrix-Nutzende von außerhalb des TU Dresden Heimatservers (Homeservers) den Raum betreten können.

![Eingabemenü für den Raumnamen](rooms3.webp)

Der Raum ist nun erstellt und erhält eine beliebige bunte Icon-Farbe.

Durch Klick auf das Zahnrad oben rechts gelangt man in die Raumeinstellungen:

![Makierung des Raumenstellungsbuttons für den neu erstellten Raum](rooms4.webp)

Hier kann im Reiter **Allgemein** ein raumspezifisches Bild/Icon hochgeladen werden. Eine wichtige Eigenschaft ist die Vergabe einer lokalen Raumadresse. Diese kann von Menschen leichter gelesen werden als die parallel auch immer vorhandene kryptische Raumadresse (die man unter dem Reiter Erweitert einsehen kann). Die vergebene lokale Raumadresse kann dann leicht in der Öffentlichkeit bzw. an die Zielgruppe verteilt werden und hat folgende Struktur:

#Raumadressname:tu-dresden.de

Eine weitere wichtige Einstellmöglichkeit ist hier, ob der Raum im Raum-Verzeichnis der TU Dresden auftauchen soll. Auch die URL-Vorschau für den Raum zu aktivieren kann hier eingestellt werden.

![Raumeinstellungen](rooms5.webp)

Im Reiter **Sicherheit & Datenschutz** sind für Raumadministrator:innen wichtige Entscheidungen zu treffen: Soll der Raum verschlüsselt werden? Wer darf Zugang erhalten? Und wer darf den bisherigen Chatverlauf lesen?

![Sicherheitseinstellungen für den neu erstellten Raum](rooms6.webp)

**Zur Erklärung der Raumzugangsoptionen:**

1. „Nur Personen, die eingeladen wurden“: Das sind geschlossene Räume. Zugang zur Zeit nur durch explizite Einladung durch Moderator:innen oder Administrator:innen
2. „Alle, denen der Raum-Link bekannt ist (ausgenommen Gäste)“: Das ist ein öffentlicher Raum, aber lesen kann man nur, wenn man ihn betritt (und damit ersichtlich wird für alle Raummitglieder). Hier kann man genau nachvollziehen, wer wann im Raum ist, und ggf. mit Kicken und Verbannen handeln, wenn Personen dabei sind, die hier nicht hingehören sollten...
3. „Alle, denen der Raum-Link bekannt ist (auch Gäste)“: Das ist ein öffentlicher Raum, und lesen können ihn alle. Weltweit. Und Raummitglieder werden nie erfahren, wer es wann gelesen hat. Dies ist also so wie eine Internetseite, auf der alle mitschreiben können. Zu dieser Einstellung oft passend wäre auch die später zu tätigende Option, dass „Jeder“ den Chatverlauf lesen darf.

Ein "Anklopfen" an geschlossene Räume ist bisher nicht möglich. Der nahestehendste Workaround ist, an die raumadministrierende Person eine Direkte Nachricht zu senden, die einen dann einlädt.

{{% notice warning %}}
Die Ende-zu-Ende-Verschlüsselung größerer oder öffentlicher Räume ist kritisch hinsichtlich der schwierigen Verifikation für viele Personen. Siehe [Ende-zu-Ende-Verschlüsselung nutzen]({{< relref "encryption" >}}).
{{% /notice %}}
{{% notice warning %}}
Als Raumadministrierende Person haben Sie die **Verantwortung** für die im Raum geteilten Inhalte (bspw. Falschnachrichten, Hetze etc.). Binden Sie weitere Personen in diese Verantwortung ein, in dem Sie in der rechten Leiste (nach Klick auf das Personensymbol) über das Drop-Down-Menü „Berechtigungslevel“ Rollen vergeben, bspw. zu Administrator:innen oder Moderator:innen.
{{% /notice %}}

![Dropdownmenü für die Rechtevergabe für Raumteilnehmende](direct_msg12.webp)

Über die Admin-Werkzeuge können Sie auch auf etwaiges Fehlverhalten reagieren (Stummschalten, Kicken, Verbannen, Kürzliche Nachrichten löschen).


