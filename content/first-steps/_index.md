---
title: "Wichtige Einstellungen"
date: 2020-07-03T13:22:13+02:00
draft: false
chapter: true
weight: 10
---
# Empfehlungen zu Schritten nach dem Erstlogin

Matrix verschlüsselt nicht nur die Transporte von und zu dem Heimserver (im Rechenzentrum der TU Dresden) sondern erlaubt auch die Nutzung von Ende-zu-Ende-Verschlüsselung (E2EE). Hierzu müssen kryptografische Schlüssel zwischen allen Geräten ausgetauscht werden, die sich Ende-zu-Ende-verschlüsselt schreiben möchten. Diese technische Notwendigkeit klingt und ist kompliziert, ist inzwischen für die Anwendenden sehr bequem geworden. Die vielen kryptografischen Schlüssel werden vom Client erstellt auf dem jeweiligen Gerät gespeichert. Sollte dies bspw. ein Tab in einem Browser sein, besteht die Gefahr, dass dieser Tab einmal unbeabsichtigt geschlossen wird. Dann sind alle verschlüsselten Inhalte nicht mehr lesbar. Damit dies nicht geschieht, wird eine Schlüsselsicherung auf dem Heimserver der TU Dresden angeboten, auf der (mit einer Passphrase (bzw. daraus errechenbaren Wiederherstellungsschlüssel) geschützt) alle kryptografischen Schlüssel verschlüsselt abgelegt sind. Es wird dringend empfohlen, diese Schlüsselsicherung zu nutzen (mit einem sicheren Passwort, welches NICHT Ihr ZIH-Passwort ist) und bei Punkt 5 weiterzulesen!
   
   ![Screenshot der Aufforderung eine Wiederherrstellungspassphrase einzugeben](02_Login5.webp)

Sollten Sie dies jetzt überspringen, sähe der nächste Bildschirm so aus:
   
   ![Bestätigung des Überspringes der Eingabe einer Passhrase](02_Login6.webp)

Die Schlüsselsicherung wird dringend empfohlen, um sorgenfrei Ende-zu-Ende-Verschlüsselung nutzen zu können. Daher wird auch nach einem weiteren Überspringen in einem kleineren Tooltip zur Einrichtung der Verschlüsselung aufgefordert:
   
   ![Chatansicht mit der Anzeige eines Tooltips, Verschlüsselung einzurichten. Makierung des bestätiten Feldes](02_Login7.webp)

Sollten Sie dies auch hier auslassen wird Ihnen eine letzte Warnung bei einem bewussten Abmelden angezeigt. Wenn spätestens hierbei keine Schlüsselsicherung eingerichtet wird, kann später auf ggf. schon stattgefundene verschlüsselte Gespräche nicht mehr zugegriffen werden.
   
   ![Abfrage, ob Nachrichten verschlüsselt werden sollen](02_Login8.webp)

Nach erfolgreicher Einrichtung der Schlüsselsicherung aktivieren Sie nun die Desktop-Benachrichtigungen:
   
   ![Screenshot der Abfrage, Pushbenarchitigungen zu aktivieren](05_Erstlogin.webp)

   Dies kann später auch rückgängig gemacht werden und insb. können einzelne „Gespräche“ in ihrer Benachrichtigungsbefugnis eingestellt werden

Einstellungen im Einstellungsmenü vornehmen: dazu ist auf die Zeile der E-Mail-Adresse und des nach unten zeigenden Dreiecks zu klicken:

![Mausklick auf das Ausklappen der Nutzendeneinstellungen, durch Klick auf das Ausklappmenü bei dem Username](06_Einstellungen1.webp)

Und anschließend auf die Zeile „Einstellungen“:

![Auswahl des Menüpunkts Einstellungen in dem Nutzendenmenü](06_Einstellungen2.webp)

In den Einstellungen können Sie im Reiter **Allgemein** bei Bedarf Ihren Anzeigenamen ändern („Vorname Nachname“) und ein Profilbild hochladen (analog zur Kontakbox auf der TUD-Website; Profilbild <5MB wählen):
![Makierung des Feldes Anzeigenamen und des Profilbilds in den Einstellungen](07_Anzeigename.webp)

Mittelfristig wird der Anzeigename aus dem Common Name im LDAP der TU Dresden erhalten, dann ist ein manuelles Ändern nicht mehr nötig.

Das E-Mail-Adressfeld ist nicht unbedingt zu füllen, da über Ihren ZIH-Login eine E-Mail-Adresse hinterlegt ist. Theoretisch können hier weitere Adressen hinzugefügt werden, bspw. um Benachrichtigungen über verpasste Nachrichten an eine weitere E-Mail-Adresse senden zu lassen.

Auf der selben Seite kann auch das Design Thema von hell zu dunkel verändert werden.

Im Reiter **Benachrichtigungen** können Sie E-Mail-Benachrichtigungen (um über verpasste Nachrichten informiert zu werden) sowie akustische Benachrichtigungen aktivieren und diese granular für einzelne Aktivitäten Anderer einstellen. Mehr dazu auf der Seite [Benachrichtigungen]({{< relref "notifications" >}}).

![Screenshot der Benarchrichtigungeneinstellungen mit einer Makierung der ausgeschalteten E-Mail benachrichtigungen](08_Benachrichtigungen.webp)

Im Reiter **Sprache & Video** können Sie den Matrix-Client Element berechtigen Ihre Medien (Kamera + Mikrofon) zu nutzen, direkten Sprach-/Videoanrufe mittels Peer-to-Peer-Verbindungen erlauben, sowie bei Videotelefonaten sich selbst in einem kleinen Bild zu sehen:

![Screenshot des Einstellungsmenüs Sprache und Video](09_Medienberechtigungen.webp)

Erfreulicherweise starten 1:1 Gespräche standardmäßig Ende-zu-Ende-verschlüsselt. Um dieser Verschlüsselung wirklich mit gutem Gefühl zu vertrauen, können Nutzer:innen den Schlüsselvergleich mit Gesprächspartner:innen durchführen. Damit dies dann auch für alle Geräte dieser Gesprächspartner:innen gilt, müssen Matrix-Nutzenden ihrerseits wiederum die Schlüssel all ihrer Geräte untereinander verifizieren (Fachbegriff: Cross-Signing). Unter Beachtung der nachfolgenden Hinweise kann dies alles sehr bequem geschehen.

Im Reiter **Sicherheit & Datenschutz** finden Sie alle Ihre Geräte, die bisher vom Matrix-Account genutzt wurden. 

* Entfernen Sie ggf. nicht mehr benutzte Sitzungen durch Markierung der quadratischen Box am Zeilenende und dem Klick auf den sich dann zeigenden roten Button.
  
![Screenshot des Menüs zum löschen von aktiven Sessions](10_Sicherheit3.webp)

* Die Geräte-Namen geben Ihnen auch Übersicht beim gegenseitigen Schlüsselvergleich zwischen Ihren Geräten. 

* Die hier vergebbaren öffentlichen Namen (durch Mausklick auf diese) Ihrer Geräte können auch von Gesprächspartner:innen eingesehen werden.  Dies hilft, wenn diese einen Vergleich der kryptografischen Schlüssel Ihrer Geräte (bspw. Laptop + Handy) durchzuführen möchten und so leicht die Gerätenamen identifizieren können.

* Die vielen kryptografischen Schlüssel werden auf dem jeweiligen Gerät gespeichert. Sollte dies bspw. ein Tab in einem Browser sein, besteht die Gefahr, dass dieser Tab einmal unbeabsichtigt geschlossen wird. Dann sind alle verschlüsselten Inhalte nicht mehr lesbar. Damit dies nicht geschieht, wird eine Schlüsselsicherung auf dem Heimserver der TU Dresden angeboten, auf der (mit einer Passphrase geschützt) alle kryptografischen Schlüssel verschlüsselt abgelegt sind. Es wird dringend empfohlen, diese Schlüsselsicherung zu nutzen!

![Screenshot des Menüpunkts zur Schlüsselsicherung](10_Sicherheit.webp)

Sofern nicht nach Erstanmeldung eingerichtet: Die **Schlüsselsicherung** ist eine wertvolle Errungenschaft, da Sie ermöglicht, die Schlüssel aller Gespräche, die Ende-zu-Ende-verschlüsselt sind, mit einem Passwort versehen zentral auf dem Server der TU Dresden zu sichern. Dies ermöglicht bequem die Nutzung mehrerer Geräte bzw. Matrix-Clients. Hierzu ist auf „Beginne Schlüsselsicherun zu nutzen“ zu klicken und eine starke Passphrase (nicht das ZIH-Passwort!!!) zu wählen. Diese Passphrase wird immer einzugeben sein, wenn Schlüssel mit  der Schlüsselsicherung synchronisiert werden sollen.

![Aufforderung eine Passwort für die Schlüsselsicherung einzugeben](11_BackupPassphrase.webp)
