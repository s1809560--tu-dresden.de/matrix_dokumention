---
title: "Nachrichten schreiben"
date: 2020-07-15T18:10:07+02:00
draft: false
chapter: true
weight: 30
---

## Personen finden und direkte Nachrichten versenden

Um einzelne Personen anzuschreiben und somit einen 1:1 Chat zu erzeugen ist als Erstes auf das + in der Kategorie „Direkte Nachrichten“ zu klicken:

![Klick au den Chat starten Button](14_Direktnachricht1.webp)

Nun ist in das Suchfeld zu tippen und bspw. die E-Mail-Adresse der empfangenen Person anzufangen zu tippen:

![Sceenshot des Suchfensters zum finden von Personen in Matrix](14_Direktnachricht2.webp)

Personen, die schon einen Account in Matrix haben sind auch durch ihren Anzeigenamen (meist „Vorname Nachname“) auffindbar. Bitte nach jedem letzten eingegebenen Zeichen 5 Sekunden warten, bis die Suchergebnisse angezeigt werden. Personen, die noch nicht eingeloggt waren, sind ausschließlich über ihre E-Mail-Adresse oder ihren ZIH-Login auffindbar. Der Link-Button „Show More“ lässt weitere Sucherergebnisse erscheinen. Beachten Sie auch, auf welchem Server Ihre gesuchte Person angezeigt wird.

{{% notice warning %}}
Ein Einfügen von E-Mail-Adressen reicht nicht aus, um Personen zu finden! Bitte geben Sie die Zeichen der E-Mail-Adresse per Hand ein.
{{% /notice %}}

Für das massenhafte Einladen von Personen (bis zu 100 auf einmal, dann wiederholbar) sind die Matrix-Namen notwendig, die in der Form @ZIH-Login:tu-dresden.de vorliegen sollten. Diese bspw. in einem Texteditor Zeile für Zeile sammeln und dann mittels Zwischenablage (Kopieren & Einfügen) in das Suchfeld in Matrix/Element einfügen.

Wenn Sie niemanden finden können, fragen Sie nach deren Benutzernamen, teilen Sie ihren Benutzernamen (@ZIH-Login:tu-dresden.de) oder https://matrix.to/#/@_Ihr_ZIH-Login_:tu-dresden.de.

Eine Einladungs-E-Mail wird nicht durch Matrix versendet.

Beachten Sie, dass Matrix-Accounts von ZIH-Funktionslogins möglicherweise nicht geprüft werden. Aufgrund der Neuheit des Mediums für Viele sowie der fehlenden Multi-Account-Funktionalität vom Matrix-Client Element, werden TU Dresden Mitarbeitende womöglich eher ihren persönlichen ZIH-Login nutzen.

Im Suchergebnis ist auf die Zielperson zu klicken:

![ein Suchergebnis auf eingegebenen Suchanfrage](14_Direktnachricht3.webp)

und dann auf Los:

![Ergebnis zugefügt zu den Personen, die in den Chat eingeladen werden](14_Direktnachricht4.webp)

Es öffnet sich das Gespräch. Ein unverschlüsseltes Gespräch kann nach Annahme der Einladung durch die verbundene Person beginnen. Die Verbindung zum Server an der TU Dresden ist verschlüsselt, jedoch sind (wie bei E-Mails und den meisten anderen Chattools) alle Inhalte innerhalb des TU Dresden Servers gespeichert. Wer eine Ende-zu-Ende-Verschlüsselung zu Gesprächspartnern wünscht, findet dazu [hier]({{< relref "encryption" >}}) weitere Informationen.

Weiteres: [Nachrichten schreiben]({{< relref "messaging" >}})

Ein Raum mit sich selbst ist auch möglich und kann als Zwischenablage / Notizbuch und für Tests benutzt werden, bspw. ob Formatierungen und Hyperlinks korrekt aussehen.

